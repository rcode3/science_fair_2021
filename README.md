# Science Fair 2021

This science fair project is about cracking passwords. This project consists of a Python program that will take
a set of passwords in a file and use three different techniques to "crack" them: brute force, dictionary attack, and
credential stuffing. This program DOES NOT target any website or computer system; all processing is done off-line.

## Brute Force

Brute force password guessing uses a computer's ability to do massively repetitive tasks. The idea behind a brute
force attack is to construct a possible set of passwords using an iterative algorithm. For example, the first
guess might be "a" and the second guess might be "aa", the third "aaa" and so on.

This project will use randomly generated guesses. For each pass, the computer will construct a guess of a random size
with a random set of characters.

## Dictionary

A dictionary attack uses a data set of common words found in the dictionary. In addition to dictionary words, the
attack can use common first and last names, in addition to target specific set of words (if the target is a set of 
passwords from a particular company, then the target file would have the company name and the names of the companies 
employees, trademarks, etc...).

The dictionary words will be used as a guess. In addition, each word will be used to construct common alterations as a
guess. For example, if the dictionary word is "foo", then both "foo" and "f00" will be used as guesses. The set of
alterations are:

* uppercasing the first character
* uppercasing any two or more consecutive characters
* substituting the character '@' for 'a'
* substituting the character '0' for 'o'
* substituting the character '$' for 's'
* appending '!' and '!!'
* appending the number sequence from 0000 to 9999

One source for a dictionary list is 
[CrackStation](https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm). Note that their
file also includes a list of cracked passwords, so it can also be used for credential stuffing (see below).

## Credential Stuffing

Credential stuffing uses a set of passwords that have been stolen from web sites and other computer systems. One
source for these passwords is [Have I Been Pwned](https://haveibeenpwned.com/Passwords). The idea behind credential
stuffing is that people do not change their passwords, and therefore it is likely you can guess their password by
knowing previous passwords they have used.

## Raspberry PI Setup

The following steps have been followed to setup Raspberry Pi's to run this software. The target Raspberry Pi platform
is the Raspberry Pi 4 8gb version with 64gb micro-sd cards. The OS used is Raspberry Pi OS 64-bit found
[here](https://downloads.raspberrypi.org/raspios_arm64/images/raspios_arm64-2021-05-28/). The image was placed on the
micro-sd card using Raspberry Pi Imager. After initial boot, the setup program is used to fully update the Raspberry Pi 
OS.

Next ssh is started and enabled with the following commands:

```shell
sudo systemctl enable ssh
sudo systemctl start ssh
sudo systemctl status ssh
```

The `ip a` command is used to get the IP address of the Pi. 

Next, Python is updated to Python 3.9 with the following steps:

```shell
wget https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tgz
tar -zxvf Python-3.9.7.tgz
cd Python-3.9.7
./configure --enable-optimizations
sudo make altinstall
python3.9 --version
```

To use the shell commands remotely, tmux was also installed: `sudo apt install tmux`.

Finally, the OS is configured to boot only in text mode using `raspi-config`.
