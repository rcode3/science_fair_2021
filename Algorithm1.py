import random
import multiprocessing


def Alg1(password):
    local_random = random.Random()
    number = 0
    found = False
    list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',',', '<', '.', '>', '/', '?', ';', ':', "'", '"', '[', '{', ']', '}', '\\', '|', '=', '+', '-', '_', ')', '(', '*', '&', '^', '%', '$', '#', '@', '!', '~', '`']
    print("Starting " +  multiprocessing.current_process().name + " with password " + password)
    nothing = ''
    while True:
      if number == 10000000:
            print('Not found')
            break

      number = number + 1
      thing = local_random.randint(1, 10)

      otherthing = local_random.choices(list, k=thing)

      otherthing = nothing.join(otherthing)

    #  print(number,otherthing)

      if(otherthing == password):
        found = True
        break

    if found == True:
        print("'" + password + "'" + ' found in ' + str(number) +  ' attempts.')

def main():
  passs = input('Password:')
  t1 = multiprocessing.Process(target=Alg1, args=(passs,))
  t2 = multiprocessing.Process(target=Alg1, args=(passs,))
  t3 = multiprocessing.Process(target=Alg1, args=(passs,))
  t4 = multiprocessing.Process(target=Alg1, args=(passs,))
  t1.start()
  t2.start()
  t3.start()
  t4.start()
  t1.join()
  t2.join()
  t3.join()
  t4.join()

if __name__ == "__main__":
  main()